class Fixnum

  ONES = {
    0 => "zero", 1 => "one", 2 => "two", 3 => "three", 4 => "four",
    5 => "five", 6 => "six", 7 => "seven", 8 => "eight", 9 => "nine"
  }.freeze

  TEENS = {
    10 => "ten", 11 => "eleven", 12 => "twelve", 13 => "thirteen",
    14 => "fourteen", 15 => "fifteen", 16 => "sixteen", 17 => "seventeen",
    18 => "eighteen", 19 => "nineteen"
  }.freeze

  TENS = {
    20 => "twenty", 30 => "thirty", 40 => "forty", 50 => "fifty",
    60 => "sixty", 70 => "seventy", 80 => "eighty", 90 => "ninety"
  }.freeze

  MAGNITUDE = {
    1 => "", 2 => "thousand", 3 => "million", 4 => "billion", 5 => "trillion"
  }.freeze

  def in_words
    arr_in_threes = self.to_s.split("").to_a.reverse.each_slice(3).to_a
    return ONES[0] if arr_in_threes.length == 1 && arr_in_threes.join.to_i == 0
    loop_through(arr_in_threes)
  end

  def loop_through(arr_in_threes)
    build_string = ""
    num_magnitude = arr_in_threes.length
    arr_in_threes.reverse.each do |threes|
      build_string += in_words_by_three(threes.join.reverse.to_i, num_magnitude)
      num_magnitude -= 1
    end
    build_string[-1] == " " ? build_string[0..-2] : build_string
  end

  def in_words_by_three(num, mag)
    value = ""
    return "" if num == 0
    case num.to_s.length
    when 1
      value = ONES[num]
    when 2
      value = last_two(num)
    else
      if num % 100 == 0
        value = "#{ONES[num / 100]} hundred"
      else
        value = "#{ONES[num / 100]} hundred #{last_two(num - (num / 100).floor * 100)}"
      end
    end
    mag == 1 ? value : "#{value} #{MAGNITUDE[mag]} "
  end

  def last_two(num)
    if num >= 10 && num <= 19
      TEENS[num]
    elsif num % 10 == 0
      TENS[num]
    else
      "#{TENS[(num / 10).floor * 10]} #{ONES[(num - (num / 10).floor * 10)]}"
    end
  end
end
